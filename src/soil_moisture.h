/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-07-19     gerhardt       the first version
 */
#ifndef SRC_SOIL_MOISTURE_H_
#define SRC_SOIL_MOISTURE_H_

int collection(void);

#endif /* SRC_SOIL_MOISTURE_H_ */
