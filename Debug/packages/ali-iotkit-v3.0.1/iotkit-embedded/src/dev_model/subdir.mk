################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_api.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_cota.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_fota.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_ipc.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_log_report.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_manager.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_message.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_message_cache.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_msg_process.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_opt.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_ota.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_utils.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/impl_linkkit.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/iotx_cm.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/iotx_cm_mqtt.c 

OBJS += \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_api.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_cota.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_fota.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_ipc.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_log_report.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_manager.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_message.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_message_cache.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_msg_process.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_opt.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_ota.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_utils.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/impl_linkkit.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/iotx_cm.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/iotx_cm_mqtt.o 

C_DEPS += \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_api.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_cota.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_fota.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_ipc.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_log_report.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_manager.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_message.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_message_cache.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_msg_process.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_opt.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_ota.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/dm_utils.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/impl_linkkit.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/iotx_cm.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/iotx_cm_mqtt.d 


# Each subdirectory must supply rules for building sources it contributes
packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/%.o: ../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/dev_model/%.c
	arm-none-eabi-gcc -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\board\ports" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\board" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\libraries\HAL_Drivers\config" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\libraries\HAL_Drivers" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model\client" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model\server" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_sign" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\infra" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\mqtt\impl" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\mqtt" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\wrappers" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\cJSON-latest" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra\arm\CMSIS_5\CMSIS\Core\Include" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra\fsp\inc\api" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra\fsp\inc\instances" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra\fsp\inc" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra_cfg\fsp_cfg\bsp" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra_cfg\fsp_cfg" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra_gen" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\dfs\include" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\drivers\include" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\finsh" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\libc\compilers\common" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\libc\compilers\newlib" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\libc\posix\io\poll" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\libc\posix\io\stdio" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\libc\posix\ipc" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\include" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\libcpu\arm\common" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\libcpu\arm\cortex-m4" -include"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rtconfig_preinc.h" -std=gnu11 -mcpu=cortex-m33 -mthumb -mfpu=fpv5-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -Dgcc -O0 -gdwarf-2 -g -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

