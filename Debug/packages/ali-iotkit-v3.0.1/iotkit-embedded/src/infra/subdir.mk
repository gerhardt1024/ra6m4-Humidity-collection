################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_cjson.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_compat.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_defs.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_log.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_net.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_prt_nwk_payload.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_report.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_sha256.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_string.c \
../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_timer.c 

OBJS += \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_cjson.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_compat.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_defs.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_log.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_net.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_prt_nwk_payload.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_report.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_sha256.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_string.o \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_timer.o 

C_DEPS += \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_cjson.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_compat.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_defs.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_log.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_net.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_prt_nwk_payload.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_report.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_sha256.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_string.d \
./packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/infra_timer.d 


# Each subdirectory must supply rules for building sources it contributes
packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/%.o: ../packages/ali-iotkit-v3.0.1/iotkit-embedded/src/infra/%.c
	arm-none-eabi-gcc -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\board\ports" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\board" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\libraries\HAL_Drivers\config" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\libraries\HAL_Drivers" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model\client" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model\server" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_model" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\dev_sign" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\infra" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\mqtt\impl" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\src\mqtt" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\ali-iotkit-v3.0.1\iotkit-embedded\wrappers" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\packages\cJSON-latest" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra\arm\CMSIS_5\CMSIS\Core\Include" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra\fsp\inc\api" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra\fsp\inc\instances" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra\fsp\inc" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra_cfg\fsp_cfg\bsp" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra_cfg\fsp_cfg" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\ra_gen" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\dfs\include" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\drivers\include" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\finsh" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\libc\compilers\common" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\libc\compilers\newlib" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\libc\posix\io\poll" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\libc\posix\io\stdio" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\components\libc\posix\ipc" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\include" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\libcpu\arm\common" -I"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rt-thread\libcpu\arm\cortex-m4" -include"D:\RT-ThreadStudio\workspace\Environmental_monitoring\rtconfig_preinc.h" -std=gnu11 -mcpu=cortex-m33 -mthumb -mfpu=fpv5-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -Dgcc -O0 -gdwarf-2 -g -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

