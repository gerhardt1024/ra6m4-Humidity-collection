/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-07-18     gerhardt       the first version
 */


#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include <string.h>
#include <stdio.h>
#include "ssd1306.h"



#define ADC_DEV_NAME        "adc0"      /* ADC 设备名称 */
#define ADC_DEV_CHANNEL     0           /* ADC 通道 */

int collection(){
        rt_adc_device_t adc_dev;
        rt_uint32_t value, vol;
        rt_err_t ret = RT_EOK;


        //初始化ssd1306
        ssd1306_Init();



            /* 查找设备 */
            adc_dev = (rt_adc_device_t)rt_device_find(ADC_DEV_NAME);
            if (adc_dev == RT_NULL)
            {
                rt_kprintf("adc sample run failed! can't find %s device!\n", ADC_DEV_NAME);
                return RT_ERROR;
            }

    while(1){
        /* 使能设备 */
        ret = rt_adc_enable(adc_dev, ADC_DEV_CHANNEL);
        /* 读取采样值 */
        value = rt_adc_read(adc_dev, ADC_DEV_CHANNEL);
        rt_kprintf("the value is :%d \n", value);

        /* 转换为对应电压值,3.3V对应12位最大值4096,数据精度乘以100保留2位小数 */
        vol = value * 330 / 4096;
        rt_kprintf("the voltage is :%d.%02d \n", vol / 100, vol % 100);
        /**
          * 湿润土壤：0-2000
         * 干燥土壤  2000-4096
         */
    if (value<2000){
        ssd1306_Fill(Black);
        ssd1306_SetCursor(18, 0);
        ssd1306_WriteString("soil moisture:", Font_7x10, White);
        ssd1306_SetCursor(40, 26);
        ssd1306_WriteString("moist", Font_11x18, White);
        ssd1306_UpdateScreen();
    } else {
        ssd1306_Fill(Black);
        ssd1306_SetCursor(18, 0);
        ssd1306_WriteString("soil moisture:", Font_7x10, White);
        ssd1306_SetCursor(45, 26);
        ssd1306_WriteString("dry", Font_11x18, White);
        ssd1306_UpdateScreen();
    }
        rt_thread_mdelay(1000);

    }
            /* 关闭通道 */
                ret = rt_adc_disable(adc_dev, ADC_DEV_CHANNEL);
        return ret;
}


/* 导出到 msh 命令列表中 */
MSH_CMD_EXPORT(collection, Soil moisture collection);
